﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    class UIDropdown
    {
        private GameObject _gameObject;

        private Dropdown UnityComponent { get { return _gameObject.GetComponent<Dropdown>(); } }
        public string Name { get; private set; }
        public List<string> Options
        {
            set
            {
                List<Dropdown.OptionData> dropdownOptions = new List<Dropdown.OptionData>();
                foreach (string val in value)
                {
                    var optionData = new UnityEngine.UI.Dropdown.OptionData();
                    optionData.text = val;
                    dropdownOptions.Add(optionData);
                }
                UnityComponent.options = dropdownOptions;
            }
        }
        public int Value { set { UnityComponent.value = value; } }

        public UIDropdown(string name, UnityAction<int> listener)
        {
            Name = name;
            _gameObject = GameObject.Find(Name).transform.Find("Dropdown").gameObject;
            UnityComponent.onValueChanged.AddListener(listener);
        }

    }
}
