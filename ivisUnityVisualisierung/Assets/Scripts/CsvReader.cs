﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Models;
using System.Linq;

/// <summary>
/// Used to read and access CSV Data
/// </summary>
public class CsvReader {

    private ColourGenerator _colorGenerator;

    public string[,] values { get; private set; }

    public List<Column> NumericColumns { get; private set; }
    public List<Column> ColorColumns { get; private set; }
    public List<string> ColorHeaders
    {
        get
        {
            List<string> data = new List<string>();
            foreach (Column col in ColorColumns)
            {
                data.Add(col.Name);
            }
            return data;
        }
    }
    public List<string> NumericHeaders
    {
        get
        {
            List<string> data = new List<string>();
            foreach (Column col in NumericColumns)
            {
                data.Add(col.Name);
            }
            return data;
        }
    }

    private int rowCount = 0;
    private int colCount = 0;

    public CsvReader (string path, char separator)
    {
        NumericColumns = new List<Column>();
        ColorColumns = new List<Column>();

        // Get the file's text.
        string fileData = System.IO.File.ReadAllText(path);

        // Split into lines.
        fileData = fileData.Replace('\n', '\r');
        string[] lines = fileData.Split(new char[] { '\r' }, System.StringSplitOptions.RemoveEmptyEntries);

        // See how many rows and columns there are.
        rowCount = lines.Length;
        colCount = lines[0].Split(separator).Length;

        // Allocate the data array.
        values = new string[rowCount, colCount];

        if (rowCount > 2)
        {
            // Load headers
            string[] types = lines[0].Split(separator);
            string[] headers = lines[1].Split(separator);
            for (int i = 0; i < colCount; i++)
            {
                if (types[i] == "i")
                    NumericColumns.Add(new Column(headers[i], i, types[i]));
                if (types[i] == "s")
                    ColorColumns.Add(new Column(headers[i], i, types[i]));
            }

            // Load values
            for (int i = 2; i < rowCount; i++)
            {
                string[] line = lines[i].Split(separator);
                for (int j = 0; j < colCount; j++)
                {
                    values[i, j] = line[j];
                }
            }
        }
    }

    /// <summary>
    /// Gets the actual data in the form of vectors
    /// </summary>
    /// <returns>The vector data.</returns>
    /// <param name="x">The column to be represented on the x coordinate.</param>
    /// <param name="y">The column to be represented on the y coordinate.</param>
    /// <param name="z">The column to be represented on the z coordinate.</param>
    public List<DataPoint> getVectorData(int x, int y, int z, int color)
    {
        _colorGenerator = new ColourGenerator();
        var colorValues = new List<string>();
        for (int i = 2; i < rowCount; i++)
        {
            colorValues.Add(values[i, ColorColumns[color].CsvColumn]);
        }
        var distinctValues = colorValues.Distinct().ToArray();

        var colorMap = new Dictionary<string, Color>();
        for (int i = 0; i < distinctValues.Length; i++)
        {
            colorMap.Add(distinctValues[i], ColorFromInt(i));
        }

        List<DataPoint> data = new List<DataPoint> ();
        for (int i = 2; i < rowCount; i++) {
            var vector = new Vector3(
                parseData(values[i, NumericColumns[x].CsvColumn]),
                parseData(values[i, NumericColumns[y].CsvColumn]),
                parseData(values[i, NumericColumns[z].CsvColumn])
            );
            data.Add(new DataPoint(vector, colorMap[values[i, ColorColumns[color].CsvColumn]]));
        }
        return data;
    }

    private float parseData(string data) {
        float result = 0;
        try {
            result = float.Parse(data);
        } catch (Exception e) {
            // ignore
        }
        return result;
    }

    private Color ColorFromInt(int index)
    {
        var hexString = _colorGenerator.NextColour();
        var color = new Color();
        ColorUtility.TryParseHtmlString("#" + hexString, out color);
        return color;
    }
}
