﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Models
{
    public class DataPoint
    {
        public Vector3 Position { get; set; }
        public Color Color { get; set; }

        public DataPoint(Vector3 position, Color color)
        {
            Position = position;
            Color = color;
        }
    }
}
