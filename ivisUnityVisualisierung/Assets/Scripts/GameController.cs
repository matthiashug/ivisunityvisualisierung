﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.UI;

/// <summary>
/// Starting point for the application
/// </summary>
public class GameController {

    private Legends legends;
    public PlotBuilder _plotBuilder;
    private CsvReader reader;

    private UIDropdown _dropdownX;
    private UIDropdown _dropdownY;
    private UIDropdown _dropdownZ;
    private UIDropdown _dropdownColor;

    public GameController () {
        // initialize stuff
        legends = new Legends ();
        reader = new CsvReader ("Assets/Resources/Data/Mappe1.csv", ',');

        _plotBuilder = new PlotBuilder(reader);
        _dropdownX = new UIDropdown("X", (int value) => { _plotBuilder.ColumnX = value; });
        _dropdownX.Options = reader.NumericHeaders;
        _dropdownX.Value = 0;
        _dropdownY = new UIDropdown("Y", (int value) => { _plotBuilder.ColumnY = value; });
        _dropdownY.Options = reader.NumericHeaders;
        _dropdownY.Value = 1;
        _dropdownZ = new UIDropdown("Z", (int value) => { _plotBuilder.ColumnZ = value; });
        _dropdownZ.Options = reader.NumericHeaders;
        _dropdownZ.Value = 2;
        _dropdownColor = new UIDropdown("Color", (int value) => { _plotBuilder.ColumnColor = value; });
        _dropdownColor.Options = reader.ColorHeaders;
        _dropdownColor.Value = 0;
    }

    public void Update () {
        // update stuff
    }
}
