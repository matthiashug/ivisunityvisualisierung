﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Draws the 3 axis with a line renderer
/// </summary>
public class Legend {

  private GameObject entity;
  private LineRenderer lineRenderer;

    public Legend(string name, Transform parent, Vector3 start, Vector3 end, Material mat) {

    entity = new GameObject(name);
    entity.transform.parent = parent;
    lineRenderer = entity.AddComponent<LineRenderer>();

    Vector3[] positions = new Vector3[]{start, end};

    lineRenderer.SetPositions(positions);
    lineRenderer.SetWidth(.05f, .05f);

    lineRenderer.material = mat;

  }

}
