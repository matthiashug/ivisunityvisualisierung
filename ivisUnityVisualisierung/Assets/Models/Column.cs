﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Models
{
    public class Column
    {
        public String Name { get; private set; }
        public int CsvColumn { get; private set; }
        public String Type { get; private set; }

        public Column(String name, int csvColumn, string type)
        {
            Name = name;
            CsvColumn = csvColumn;
            Type = type;
        }
    }
}
