﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Creates 3 lines to represent the 3 axis
/// </summary>
public class Legends {

  private GameObject entity;
  private Legend legendX, legendY, legendZ;

  public Legends() {
        entity = new GameObject("Legend");
        legendX = new Legend("Legend X", entity.transform, Vector3.left, Vector3.right * 100, Resources.Load("Materials/LegendX") as Material);
        legendY = new Legend("Legend Y", entity.transform, Vector3.down, Vector3.up * 100, Resources.Load("Materials/LegendY") as Material);
        legendZ = new Legend("Legend Z", entity.transform, Vector3.back, Vector3.forward * 100, Resources.Load("Materials/LegendZ") as Material);
  }
}
