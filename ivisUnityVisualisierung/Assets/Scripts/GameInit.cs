﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Main MonoBehavior.
/// Creates our game controller and forwards unity methods to it.
/// </summary>
public class GameInit : MonoBehaviour {

    private GameController controller;

    void Start () {
        controller = new GameController ();
    }

    // Update is called once per frame
    void Update () {
        controller.Update ();
    }

    // ugly way to access the plot builder because we chose the lazy,
    // editor only way to create the UI
    public void setScaleX (Slider slider) {
        controller._plotBuilder.FactorX = slider.value;
    }

    public void setScaleY (Slider slider) {
        controller._plotBuilder.FactorY = slider.value;
    }

    public void setScaleZ (Slider slider) {
        controller._plotBuilder.FactorZ = slider.value;
    }
}
