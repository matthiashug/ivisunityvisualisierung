﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Models;

/// <summary>
/// Displays the given data in the 3D space with gameObjects
/// </summary>
public class PlotBuilder {

    private GameObject entity;

    private List<GameObject> plotObjects;

    private CsvReader _reader;
    private int _columnX = 1, _columnY = 2, _columnZ = 3, _columnColor = 0;
    private int startX = 0, startY = 0, startZ = 0;
    private float factorX = 0.1f, factorY = 0.1f, factorZ = 0.1f;

    public float FactorX
    {
        get { return factorX; }
        set
        {
            factorX = value;
            generatePlot();
        }
    }

    public float FactorY
    {
        get { return factorY; }
        set
        {
            factorY = value;
            generatePlot();
        }
    }

    public float FactorZ
    {
        get { return factorZ; }
        set
        {
            factorZ = value;
            generatePlot();
        }
    }

    public int ColumnX { get { return _columnX; } set { _columnX = value; generatePlot(); } }
    public int ColumnY { get { return _columnY; } set { _columnY = value; generatePlot(); } }
    public int ColumnZ { get { return _columnZ; } set { _columnZ = value; generatePlot(); } }
    public int ColumnColor { get { return _columnColor; } set { _columnColor = value; generatePlot(); } }

    public PlotBuilder (CsvReader reader) {
        _reader = reader;
        plotObjects = new List<GameObject>();
        entity = new GameObject("Plot");
        generatePlot();
    }

    private void generatePlot () {

        foreach (GameObject gmObj in plotObjects) {
            Object.Destroy (gmObj);
        }
        plotObjects = new List<GameObject> ();

        var data = _reader.getVectorData(_columnX, _columnY, _columnZ, _columnColor);
        foreach (DataPoint entry in data) {
            Vector3 position = new Vector3 (
                             (entry.Position.x - startX) * factorX,
                             (entry.Position.y - startY) * factorY,
                             (entry.Position.z - startZ) * factorZ
                         );

            GameObject gmObj = GameObject.CreatePrimitive (PrimitiveType.Sphere);
            gmObj.transform.position = position;
            gmObj.transform.localScale *= 0.5f;
            gmObj.transform.parent = entity.transform;
            gmObj.GetComponent<Renderer>().material.color = entry.Color;
            plotObjects.Add (gmObj);
        }
    }

    public void updateData() {
        generatePlot ();
    }
}


